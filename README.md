# INMEVO
The International Meteorological Vocabulary (https://public.wmo.int/fr/ressources/meteoterm) is a terminology resource published by the World Meteorological Organization (WMO), first in 1966 and then in 1992 in an updated and expanded version. The objective was to standardize the terminology and to facilitate communication between experts of different languages. 

The 1992 version, the most recent to date, describes about 3500 meteorological terms with their definitions, in 4 languages: English (EN), French (FR), Russian (RU) and Spanish (ES). These terms have been approved by different organizations such as WMO members, the International Civil Aviation or the International Commission on Illumination. Although this manual lacks current terms, e.g. related to remote sensing or climate change, it provides a solid base of knowledge for understanding meteorological terms or interpreting data sets in the field.

INMEVO is a structured (SKOS) version of the International Meteorological Vocabulary and available in two languages (EN and FR). 

## License
This resource is available under MIT licence.

## Project status
The resource is under development. 
